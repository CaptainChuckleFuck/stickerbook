from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import json

_backgroundImagePath = "Stickers/background.jpg"
_placeholderImagePath = "Stickers/StickerPlaceholder.png"
_stickersFolderPath = "Stickers/"
_outputFile = "output.jpg"
_straightPlaceholder = True

# Opening JSON file
f = open('data.json',)  
# returns JSON object as a dictionary
data = json.load(f)
# Closing file
f.close()

font = ImageFont.truetype("COOPBL.ttf", 16)

def AlphaRotate(image, rotationAngle):
    convertedImage = image.convert('RGBA')
    convertedImage = convertedImage.rotate(rotationAngle, expand=1)
    newImg = Image.new('RGBA', convertedImage.size, (255,255,255,0))
    alphaImage = Image.composite(convertedImage, newImg, convertedImage)
    return alphaImage

# get default placeholder and background from file, ensure they are alpha compatable by RGBA convert
imPlaceholder = Image.open(_placeholderImagePath).convert("RGBA")
imBackground = Image.open(_backgroundImagePath).convert("RGBA")
for sticker in data['stickers']:
    # they are owned use the actual sticker, else use the placeholder    
    rotationAngle = 0
    if(_straightPlaceholder is False):
            rotationAngle = sticker["rotation"]
    alphaImage = AlphaRotate(imPlaceholder, rotationAngle)
    imBackground.paste(alphaImage, (sticker["position"][0],sticker["position"][1]), mask=alphaImage)

    # if owned, pasted sticker over the top
    if(sticker["isOwned"]):
        stickerPath = str(_stickersFolderPath)+str(sticker["id"])+".png"
        imStickerImage = Image.open(stickerPath).convert("RGBA") # load the correct sticker for this slot
        alphaImage = AlphaRotate(imStickerImage, sticker["rotation"])
        imBackground.paste(alphaImage, (sticker["position"][0] + sticker["offset"][0],sticker["position"][1] + sticker["offset"][1]),  mask=alphaImage)

for sticker in data['stickers']:
    draw = ImageDraw.Draw(imBackground) 
    draw.text((sticker["textPos"][0],sticker["textPos"][1]), sticker["name"], font = font, align ="centre") 

imBackground.show()
imBackground = imBackground.convert("RGB")
imBackground.save(_outputFile)