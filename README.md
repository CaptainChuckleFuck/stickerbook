# StickerBook

Application for programmatically generating a 'stickerbook'
Composite 'owned' stickers onto a stickerbook at the locations described in the data.json file

INSTALLATION
Required the python PILLOW library: https://pillow.readthedocs.io/en/stable/installation.html
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade Pillow

USAGE:
The background image (/Stickers/background.jpg) is used as the base to build the output image upon, this can be replaced with a different desired background.
The placeholder image (/Stickers/StickerPlaceholder.png) are placed ontop of the background image to indicate a space that still needs to be filled
NOTE: Exact filename and format should be maintained when replacing the background and placeholder images

The data.json file describes each sticker 'spot' within the stickerbook, each space can be configured in the following ways:
id - a unique identifiers for the space, this matches a .png file within the stickers folder
name - optional writing to describe the sticker, can be left blank ""
isOwned - whether this space should be a blank placeholder or the sticker itself 
position - co-ordinates where the sticker / placeholder are pasted onto the background image
rotation - how much the position should be rotated
textPos - where to write the spots 'name' field and the background image
offset - an adjustment to the position of the sticker compared to the placeholder space